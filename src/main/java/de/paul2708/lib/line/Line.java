package de.paul2708.lib.line;

import de.paul2708.lib.position.LinePosition;
import de.paul2708.lib.util.LinearSystem;
import de.paul2708.lib.vector.Vector;
import de.paul2708.lib.point.Point;

/**
 * Created by Paul on 23.06.2016.
 */
public class Line {

    private Vector supportVector;
    private Vector directionVector;

    public Line(Vector supportVector, Vector directionVector) {
        this.supportVector = supportVector;
        this.directionVector = directionVector;
    }

    public Line(Point a, Point b) {
        this(a.getStationaryVector(), Vector.get(b.getX1() - a.getX1(), b.getX2() - a.getX2(), b.getX3() - a.getX3()));
    }

    public LinePosition.Result getPosition(Line line) {
        return new LinePosition(this, line).resolve();
    }

    public Point intersect(Line line) {
        if (new LinePosition(this, line).resolve() != LinePosition.Result.INTERSECTION) {
            throw new NullPointerException("no intersection");
        }

        Vector g1Dir = getDirectionVector();
        Vector g2Dir = line.getDirectionVector();
        double[][] matrix = new double[][] { { g1Dir.getX1(), -g2Dir.getX1(), line.getSupportVector().getX1() - getSupportVector().getX1() },
                { g1Dir.getX2(), -g2Dir.getX2(), line.getSupportVector().getX2() - getSupportVector().getX2() },
                { g1Dir.getX3(), -g2Dir.getX3(), line.getSupportVector().getX3() - getSupportVector().getX3() }
        };
        LinearSystem.toRREF(matrix);

        double t = 1/matrix[0][0] * matrix[0][2];
        double t2 = 1/matrix[1][1] * matrix[1][2];

        // Probe
        if (g1Dir.getX1() * t - g2Dir.getX1() * t2 == line.getSupportVector().getX1() - getSupportVector().getX1() &&
                g1Dir.getX2() * t - g2Dir.getX2() * t2 == line.getSupportVector().getX2() - getSupportVector().getX2() &&
                g1Dir.getX3() * t - g2Dir.getX3() * t2 == line.getSupportVector().getX3() - getSupportVector().getX3()) {
            return Point.get(t * g1Dir.getX1() + getSupportVector().getX1(),
                    t * g1Dir.getX2() + getSupportVector().getX2(),
                    t * g1Dir.getX3() + getSupportVector().getX3());
        } else {
            return null;
        }
    }

    public Point intersect() {
        // TODO: todo
        return null;
    }

    public Point getPoint(double t) {
        double x1 = supportVector.getX1() + t * directionVector.getX1();
        double x2 = supportVector.getX2() + t * directionVector.getX2();
        double x3 = supportVector.getX3() + t * directionVector.getX3();

        return Point.get(x1, x2, x3);
    }

    public Vector getSupportVector() {
        return supportVector;
    }

    public Vector getDirectionVector() {
        return directionVector;
    }

    public String getTerm() {
        return "g: x = (" + supportVector.getX1() + "|" + supportVector.getX2() + "|" + supportVector.getX3() +
            ") + t*(" + directionVector.getX1() + "|" + directionVector.getX2() + "|" + directionVector.getX3() +
            ")";
    }

    @Override
    public String toString() {
        return getTerm();
    }

    public static void main(String[] args) {
        Line g1 = new Line(new Vector(5, 3, 1), new Vector(1, 0, 2));
        Line g2 = new Line(new Vector(0, 1, 1), new Vector(3, 1, 1));

        System.out.println(g1.intersect(g2).getStationaryVector().toString());
    }
}
