package de.paul2708.lib.plane;

import de.paul2708.lib.line.Line;
import de.paul2708.lib.point.Point;
import de.paul2708.lib.point.PointProbe;
import de.paul2708.lib.position.LinePosition;
import de.paul2708.lib.position.Position;
import de.paul2708.lib.util.LinearSystem;
import de.paul2708.lib.vector.Vector;

/**
 * Created by Paul on 16.09.2016.
 */
public class ParameterPlane implements Plane {

    private Vector supportVector;
    private Vector rSpanVector;
    private Vector sSpanVector;

    public ParameterPlane(Vector supportVector, Vector rSpanVector, Vector sSpanVector) {
        this.supportVector = supportVector;
        this.rSpanVector = rSpanVector;
        this.sSpanVector = sSpanVector;
    }

    public ParameterPlane(Point a, Point b, Point c) {
        if (new Line(a, b).getSupportVector().equals(new Line(a, c).getDirectionVector()) &&
                new Line(a, b).getDirectionVector().equals(new Line(b, c).getDirectionVector())) {
            throw new IllegalArgumentException("all points are located on a line");
        }

        this.supportVector = a.getStationaryVector();
        this.rSpanVector = a.getStationaryVector().subtract(b.getStationaryVector());
        this.sSpanVector = a.getStationaryVector().subtract(c.getStationaryVector());
    }

    public ParameterPlane(Line g, Point p) {
        if (new PointProbe(g, p).calculate() == PointProbe.Result.ON_LINE) {
            throw new IllegalArgumentException("the point cannot be located on the line");
        }

        this.supportVector = g.getSupportVector();
        this.rSpanVector = g.getDirectionVector();
        this.sSpanVector = p.getStationaryVector().subtract(g.getSupportVector());
    }

    public ParameterPlane(Line g1, Line g2) {
        if (new LinePosition(g1, g2).resolve() != LinePosition.Result.PARALLEL) {
            throw new IllegalArgumentException("g1 is not parallel to g2");
        }

        this.supportVector = g1.getSupportVector();
        this.rSpanVector = g1.getDirectionVector();
        this.sSpanVector = g2.getSupportVector().subtract(g1.getSupportVector());
    }

    public Point getPoint(double r, double s) {
        double x1 = supportVector.getX1() + r * rSpanVector.getX1() + s * sSpanVector.getX1();
        double x2 = supportVector.getX2() + r * rSpanVector.getX2() + s * sSpanVector.getX2();
        double x3 = supportVector.getX3() + r * rSpanVector.getX3() + s * sSpanVector.getX3();

        return Point.get(x1, x2, x3);
    }

    @Override
    public Position position(Line line) {
        Vector sup = line.getSupportVector();
        Vector dir = line.getDirectionVector();
        double[][] matrix = new double[][] {
                { -rSpanVector.getX1(), -sSpanVector.getX1(), dir.getX1(), supportVector.getX1() - sup.getX1() },
                { -rSpanVector.getX2(), -sSpanVector.getX2(), dir.getX2(), supportVector.getX2() - sup.getX2() },
                { -rSpanVector.getX3(), -sSpanVector.getX3(), dir.getX3(), supportVector.getX3() - sup.getX3() }
        };
        LinearSystem.toRREF(matrix);

        Position position = null;
        LinearSystem.Result result = LinearSystem.resolve(matrix);
        if (result == LinearSystem.Result.INFINITY_SOLUTION) {
            position =  Position.ON_PLAIN;
        } else if (result == LinearSystem.Result.NO_SOLUTION) {
            position = Position.PARALLEL;
        } else if (result == LinearSystem.Result.SOLVABLE) {
            position = Position.INTERSECTION;
        }

        return position;
    }

    @Override
    public Point intersect(Line line) {
        if (position(line) != Position.INTERSECTION) {
            throw new IllegalArgumentException("there is no intersection");
        }

        Vector sup = line.getSupportVector();
        Vector dir = line.getDirectionVector();
        double[][] matrix = new double[][] {
                { -rSpanVector.getX1(), -sSpanVector.getX1(), dir.getX1(), supportVector.getX1() - sup.getX1() },
                { -rSpanVector.getX2(), -sSpanVector.getX2(), dir.getX2(), supportVector.getX2() - sup.getX2() },
                { -rSpanVector.getX3(), -sSpanVector.getX3(), dir.getX3(), supportVector.getX3() - sup.getX3() }
        };
        LinearSystem.toRREF(matrix);

        return line.getPoint(matrix[2][3]);
    }

    @Override
    public Position position(Plane plane) {
        // TODO: Other planes
        Position position = null;
        switch (plane.getFormat()) {
            case PARAMETER_FORMAT:
                ParameterPlane parameterPlane = (ParameterPlane) plane;
                Vector sup = parameterPlane.getSupportVector();
                Vector r = parameterPlane.getrSpanVector();
                Vector s = parameterPlane.getsSpanVector();

                double[][] matrix = new double[][] {
                        { rSpanVector.getX1(), sSpanVector.getX1(), r.getX1(), s.getX1(), sup.getX1() - supportVector.getX1() },
                        { rSpanVector.getX2(), sSpanVector.getX2(), r.getX2(), s.getX2(), sup.getX2() - supportVector.getX2() },
                        { rSpanVector.getX3(), sSpanVector.getX3(), r.getX3(), s.getX3(), sup.getX3() - supportVector.getX3() }
                };
                LinearSystem.toRREF(matrix);

                LinearSystem.Result result = LinearSystem.resolve(matrix);
                if (result == LinearSystem.Result.INFINITY_SOLUTION) {
                    position =  Position.IDENTICAL;
                } else if (result == LinearSystem.Result.NO_SOLUTION) {
                    position = Position.PARALLEL;
                } else if (result == LinearSystem.Result.SOLVABLE) {
                    position = Position.INTERSECTION;
                }

                break;
            case COORDINATION_FORMAT:
                
                break;
            case NORMALEN_FORMAT:

                break;
        }

        return position;
    }

    @Override
    public Line intersect(Plane plane) {
        if (position(plane) != Position.INTERSECTION) {
            throw new IllegalArgumentException("there is no intersection");
        }
        // TODO: Other planes
        Line line = null;
        switch (plane.getFormat()) {
            case PARAMETER_FORMAT:
                ParameterPlane parameterPlane = (ParameterPlane) plane;
                Vector sup = parameterPlane.getSupportVector();
                Vector rVec = parameterPlane.getrSpanVector();
                Vector sVec = parameterPlane.getsSpanVector();

                double[][] matrix = new double[][] {
                        { rSpanVector.getX1(), sSpanVector.getX1(), rVec.getX1(), sVec.getX1(), supportVector.getX1() - sup.getX1() },
                        { rSpanVector.getX2(), sSpanVector.getX2(), rVec.getX2(), sVec.getX2(), supportVector.getX2() - sup.getX2() },
                        { rSpanVector.getX3(), sSpanVector.getX3(), rVec.getX3(), sVec.getX3(), supportVector.getX3() - sup.getX3() }
                };
                LinearSystem.toRREF(matrix);

                Vector lineSupport = parameterPlane.getSupportVector().add(rVec.multiply(matrix[2][4]));
                Vector lineDirection = parameterPlane.getsSpanVector().add(rVec, -matrix[2][3]);

                line = new Line(lineSupport, lineDirection);
                break;
            case COORDINATION_FORMAT:

                break;
            case NORMALEN_FORMAT:

                break;
        }

        return line;
    }

    @Override
    public boolean isOnPlain(Point point) {
        double[][] matrix = new double[][] {
                { rSpanVector.getX1(), sSpanVector.getX1(), point.getX1() - supportVector.getX1() },
                { rSpanVector.getX2(), sSpanVector.getX2(), point.getX2() - supportVector.getX2() },
                { rSpanVector.getX3(), sSpanVector.getX3(), point.getX3() - supportVector.getX3() }
        };
        LinearSystem.toRREF(matrix);

        return LinearSystem.resolve(matrix) != LinearSystem.Result.NO_SOLUTION;
    }

    @Override
    public Plane transform(PlaneFormat planeFormat) {
        Plane transfer = null;
        switch (planeFormat) {
            case PARAMETER_FORMAT:
                transfer = this;
                break;
            case COORDINATION_FORMAT:
                Point a = getPoint(0, 0);
                Point b = getPoint(1, 0);
                Point c = getPoint(1, 1);

                double[][] matrix = new double[][] {
                        { a.getX1(), a.getX2(), a.getX3(), -1, 0 },
                        { b.getX1(), b.getX2(), b.getX3(), -1, 0 },
                        { c.getX1(), c.getX2(), c.getX3(), -1, 0 },
                };
                LinearSystem.toRREF(matrix);

                double dCoord = 1.0/matrix[2][3] * (0 - 1);
                double bCoord = -matrix[1][3] * dCoord;
                double aCoord = -matrix[0][3] * dCoord;

                transfer = new CoordinatePlane(aCoord, bCoord, 1, dCoord);
                break;
            case NORMALEN_FORMAT:

                break;
            default:
                break;
        }

        return transfer;
    }

    @Override
    public PlaneFormat getFormat() {
        return PlaneFormat.PARAMETER_FORMAT;
    }

    @Override
    public String getTerm() {
        return "E: x = (" + supportVector.getX1() + "|" + supportVector.getX2() + "|" + supportVector.getX3() +
                ") + r*(" + rSpanVector.getX1() + "|" + rSpanVector.getX2() + "|" + rSpanVector.getX3() + ") + s*(" +
                sSpanVector.getX1() + "|" + sSpanVector.getX2() + "|" + sSpanVector.getX3() + ")";
    }

    public Vector getSupportVector() {
        return supportVector;
    }

    public Vector getrSpanVector() {
        return rSpanVector;
    }

    public Vector getsSpanVector() {
        return sSpanVector;
    }

    public static void main(String[] args) {
        ParameterPlane plane = new ParameterPlane(Vector.get(1, 2, 0), Vector.get(1, 0, 1), Vector.get(1, 2, 3));
        System.out.println(plane.transform(PlaneFormat.COORDINATION_FORMAT).getTerm());
    }
}
