package de.paul2708.lib.plane;

import de.paul2708.lib.line.Line;
import de.paul2708.lib.point.Point;
import de.paul2708.lib.position.Position;

/**
 * Created by Paul on 28.06.2016.
 */
public interface Plane {

    Position position(Line line);

    Point intersect(Line line);

    Position position(Plane plane);

    Line intersect(Plane plane);

    boolean isOnPlain(Point point);

    Plane transform(PlaneFormat planeFormat);

    PlaneFormat getFormat();

    String getTerm();

}
