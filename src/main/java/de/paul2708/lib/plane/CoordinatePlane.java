package de.paul2708.lib.plane;

import de.paul2708.lib.line.Line;
import de.paul2708.lib.point.Point;
import de.paul2708.lib.position.Position;
import de.paul2708.lib.vector.Vector;

import static javafx.scene.input.KeyCode.X;

/**
 * Created by Paul on 27.09.2016.
 */
public class CoordinatePlane implements Plane {

    private double a;
    private double b;
    private double c;
    private double d;

    public CoordinatePlane(double a, double b, double c, double d) {
        if (a == 0 && b == 0 && c == 0) {
            throw new IllegalArgumentException("not all coefficients can be zero");
        }

        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public Point getPoint(Coordinate first, double firstCoord, Coordinate second, double secondCoord) {
        if (first == second) {
            throw new IllegalArgumentException("two similar coordiantes");
        }

        switch (first) {
            case X1:
                double pointX3;
                if (second == Coordinate.X2) {
                    pointX3 = (1.0/c) * (d - (b * secondCoord) - (a * firstCoord));
                    return Point.get(firstCoord, secondCoord, pointX3);
                } else {
                    pointX3 = (1.0/c) * (d - (b * firstCoord) - (a * secondCoord));
                    return Point.get(secondCoord, firstCoord, pointX3);
                }
            case X2:
                break;
            case X3:
                break;
        }

        if ((first == Coordinate.X2 && second == Coordinate.X3) || (first == Coordinate.X3 && second == Coordinate.X2)) {
            double pointX1 = (1.0/a) * (d - (c * pointSecond) - (b * pointFirst));
            return Point.get(pointX1, pointFirst, pointSecond);
        } else if ((first == Coordinate.X1 && second == Coordinate.X3) || (first == Coordinate.X3 && second == Coordinate.X1)) {
            double pointX2 = (1.0/b) * (d - (c * pointSecond) - (a * pointFirst));
            return Point.get(pointFirst, pointFirst, pointSecond);
        } else if ((first == Coordinate.X1 && second == Coordinate.X2) || (first == Coordinate.X2 && second == Coordinate.X1)) {
            double pointX1 = (1.0/a) * (d - (c * pointSecond) - (b * pointFirst));
            return Point.get(pointX1, pointFirst, pointSecond);
        } else {
            return null;
        }
    }

    public CoordinatePlane multiply(double factor) {
        return new CoordinatePlane(factor * a, factor * b, factor * c, factor * d);
    }

    public CoordinatePlane rational() {
        double factor = 1;

        for (int i = 0; i < getCoefficients().length; i++) {
            double coord = getCoefficients()[i];
            if (coord % 2 == 0 || coord % 2 == 1) {
                continue;
            }

            factor *= 1.0/coord;
        }

        return multiply(factor);
    }

    @Override
    public Position position(Line line) {

        return null;
    }

    @Override
    public Point intersect(Line line) {
        if (position(line) != Position.INTERSECTION) {
            throw new IllegalArgumentException("there is no intersection");
        }

        return null;
    }

    @Override
    public Position position(Plane plane) {
        return null;
    }

    @Override
    public Line intersect(Plane plane) {
        return null;
    }

    @Override
    public boolean isOnPlain(Point point) {
        double result = a * point.getX1() + b * point.getX2() + c * point.getX3();
        return result == d;
    }

    @Override
    public Plane transform(PlaneFormat planeFormat) {
        Plane transfer = null;
        switch (planeFormat) {
            case PARAMETER_FORMAT:
                if (a != 0) {
                    double planeX = d/a;
                    double planeY = -b/a;
                    double planeZ = -c/a;

                    transfer = new ParameterPlane(Vector.get(planeX, 0, 0), Vector.get(planeY, 1, 0),
                            Vector.get(planeZ, 0, 1));
                }

                break;
            case COORDINATION_FORMAT:
                transfer = this;
                break;
            case NORMALEN_FORMAT:

                break;
            default:
                break;
        }

        return transfer;
    }

    @Override
    public PlaneFormat getFormat() {
        return PlaneFormat.COORDINATION_FORMAT;
    }

    @Override
    public String getTerm() {
        return "E: " +
                (a == 0 ? "" : a + "x1") + " " +
                (b == 0 ? "" : ((b > 0) ? "+ " : "- ") + Math.abs(b) + "x2") + " " +
                (c == 0 ? "" : ((c > 0) ? "+ " : "- ") + Math.abs(c) + "x3") + " = " + d;
    }

    public double[] getCoefficients() {
        return new double[] { a, b, c, d };
    }

    public enum Coordinate {

        X1,
        X2,
        X3;
    }

    public static void main(String[] args) {
        CoordinatePlane plane = new CoordinatePlane(1, 2, 3, 5);
        System.out.println(plane.getPoint(Coordinate.X2, Coordinate.X3, 1, 0.5));

    }
}
