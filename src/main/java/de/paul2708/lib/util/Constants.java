package de.paul2708.lib.util;

import de.paul2708.lib.line.Line;
import de.paul2708.lib.point.Point;
import de.paul2708.lib.vector.Vector;

/**
 * Created by Paul on 07.07.2016.
 */
public class Constants {

    public static final Point ORIGIN = Point.get(0, 0, 0);

    public static final Line X1_AXE = new Line(ORIGIN.getStationaryVector(), Vector.get(1, 0, 0));
    public static final Line X2_AXE = new Line(ORIGIN.getStationaryVector(), Vector.get(0, 1, 0));
    public static final Line X3_AXE = new Line(ORIGIN.getStationaryVector(), Vector.get(0, 0, 1));
}
