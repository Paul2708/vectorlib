package de.paul2708.lib.util;

import java.util.Arrays;

/**
 * Created by Paul on 28.06.2016.
 */
public class LinearSystem {

    public static void toRREF(double[][] M) {
        int rowCount = M.length;
        if (rowCount == 0)
            return;

        int columnCount = M[0].length;

        int lead = 0;
        for (int r = 0; r < rowCount; r++) {
            if (lead >= columnCount)
                break;
            {
                int i = r;
                while (M[i][lead] == 0) {
                    i++;
                    if (i == rowCount) {
                        i = r;
                        lead++;
                        if (lead == columnCount)
                            return;
                    }
                }
                double[] temp = M[r];
                M[r] = M[i];
                M[i] = temp;
            }

            {
                double lv = M[r][lead];
                for (int j = 0; j < columnCount; j++)
                    M[r][j] /= lv;
            }

            for (int i = 0; i < rowCount; i++) {
                if (i != r) {
                    double lv = M[i][lead];
                    for (int j = 0; j < columnCount; j++)
                        M[i][j] -= lv * M[r][j];
                }
            }
            lead++;
        }
    }

    public static Result resolve(double[][] matrix) {
        int lastRow = matrix.length - 1;
        int column = matrix[0].length;
        int zero = 0;

        for (int i = 0; i < column - 1; i++) {
            if (matrix[lastRow][i] == 0) {
                zero++;
            }
        }
        if (zero == column - 1) {
            if (matrix[lastRow][column-1] == 0) {
                return Result.INFINITY_SOLUTION;
            } else {
                return Result.NO_SOLUTION;
            }
        }

        return Result.SOLVABLE;
    }

    public enum Result {

        INFINITY_SOLUTION,
        NO_SOLUTION,
        SOLVABLE;
    }

    public static void main(String[] args) {
        double[][] mtx = {
                { 1, 3, -1, 4},
                { 2, 1, 1, 7},
                {2, -4, 4, 6} };

        toRREF(mtx);
        System.out.println(Arrays.deepToString(mtx));
        System.out.println(resolve(mtx));

        double[][] matrix = {
                { -7, 1, 5, -1, 0},
                { -5, 2, 6, -1, 0},
                {-11, 2, 4, -1, 0} };
        toRREF(matrix);
        System.out.println(Arrays.deepToString(matrix));
        System.out.println(resolve(matrix));
    }
}
