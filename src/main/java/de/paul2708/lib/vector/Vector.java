package de.paul2708.lib.vector;

import de.paul2708.lib.point.Point;

/**
 * Created by Paul on 23.06.2016.
 */
public class Vector {

    private double x1;
    private double x2;
    private double x3;

    private double length;

    public Vector(double x1, double x2, double x3) {
        this.x1 = x1;
        this.x2 = x2;
        this.x3 = x3;

        this.length = Double.NaN;
    }

    public Vector(Point a, Point b) {
        this(b.getX1() - a.getX1(), b.getX2() - a.getX2(), b.getX3() - a.getX3());
    }

    public Vector add(Vector vector) {
        return new Vector(vector.getX1() + x1,
                vector.getX2() + x2, vector.getX3() + x3);
    }

    public Vector add(Vector vector, double times) {
        return new Vector((times * vector.getX1()) + x1,
                (times * vector.getX2()) + x2, (times * vector.getX3()) + x3);
    }

    public Vector subtract(Vector vector) {
        return new Vector(x1 - vector.getX1(), x2 - vector.getX2(), x3 - vector.getX3());
    }

    public Vector multiply(double k) {
        return new Vector(k * x1, k * x2, k * x3);
    }

    public Vector getOppositeVector() {
        return new Vector(-x1, -x2, -x3);
    }

    public double scalar(Vector vector) {
        return new ScalarProduct(this, vector).get();
    }

    public double getX1() {
        return x1;
    }

    public double getX2() {
        return x2;
    }

    public double getX3() {
        return x3;
    }

    public double getLength() {
        if (Double.isNaN(length)) {
            length = Math.sqrt(Math.pow(x1, 2) + Math.pow(x2, 2) + Math.pow(x3, 2));
        }

        return length;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Vector) {
            if (this.getX1() == ((Vector) obj).getX1() &&
                    this.getX2() == ((Vector) obj).getX2() &&
                    this.getX3() ==  ((Vector) obj).getX3()) {
                return true;
            }
        }

        return false;
    }

    @Override
    public String toString() {
        return "Vector{" +
                "x1=" + x1 +
                ", x2=" + x2 +
                ", x3=" + x3 +
                ", length=" + length +
                '}';
    }

    public static Vector get(Point a, Point b) {
        return new Vector(a, b);
    }

    public static Vector get(double x1, double x2, double x3) {
        return new Vector(x1, x2, x3);
    }
}
