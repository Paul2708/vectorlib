package de.paul2708.lib.vector;

/**
 * Created by Paul on 16.09.2016.
 */
public class ScalarProduct {

    private Vector a;
    private Vector b;

    private double product;

    public ScalarProduct(Vector a, Vector b) {
        this.a = a;
        this.b = b;

        this.product = Double.NaN;
    }

    public double get() {
        if (Double.isNaN(product)) {
            product = a.getX1() * b.getX1() + a.getX2() * b.getX2() + a.getX3() * b.getX3();
        }

        return product;
    }
}
