package de.paul2708.lib.vector;

import de.paul2708.lib.point.Point;

/**
 * Created by Paul on 23.06.2016.
 */
public class StationaryVector extends Vector {

    public StationaryVector(Point a) {
        super(new Point(0, 0, 0), a);
    }
}
