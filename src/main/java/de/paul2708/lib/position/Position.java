package de.paul2708.lib.position;

/**
 * Created by Paul on 25.09.2016.
 */
public enum Position {

    INTERSECTION,
    PARALLEL,
    IDENTICAL,
    SKEW,
    ON_PLAIN;
}
