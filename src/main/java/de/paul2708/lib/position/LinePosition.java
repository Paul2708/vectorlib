package de.paul2708.lib.position;

import de.paul2708.lib.line.Line;
import de.paul2708.lib.util.LinearSystem;
import de.paul2708.lib.vector.Vector;
import de.paul2708.lib.point.Point;
import de.paul2708.lib.point.PointProbe;

/**
 * Created by Paul on 23.06.2016.
 */
public class LinePosition {

    private Line g1;
    private Line g2;

    public LinePosition(Line g1, Line g2) {
        this.g1 = g1;
        this.g2 = g2;
    }

    public Result resolve() {
        // Parallel oder identisch
        double k = g1.getDirectionVector().getX1() / g2.getDirectionVector().getX1();

        if (k * g2.getDirectionVector().getX2() == g1.getDirectionVector().getX2() &&
                k * g2.getDirectionVector().getX3() == g1.getDirectionVector().getX3()) {
            Point point = new Point(g2.getSupportVector().getX1(), g2.getSupportVector().getX2(),
                    g2.getSupportVector().getX3());
            PointProbe probe = new PointProbe(g1, point);
            if (probe.calculate() == PointProbe.Result.ON_LINE) {
                return Result.IDENTICAL;
            } else {
                return Result.PARALLEL;
            }
        }
        // Schnittpunkt
        Vector g1Dir = g1.getDirectionVector();
        Vector g2Dir = g2.getDirectionVector();
        double[][] matrix = new double[][] { { g1Dir.getX1(), -g2Dir.getX1(), g2.getSupportVector().getX1() - g1.getSupportVector().getX1() },
                                             { g1Dir.getX2(), -g2Dir.getX2(), g2.getSupportVector().getX2() - g1.getSupportVector().getX2() },
                                             { g1Dir.getX3(), -g2Dir.getX3(), g2.getSupportVector().getX3() - g1.getSupportVector().getX3() }
                                           };
        LinearSystem.toRREF(matrix);
        if (matrix[2][0] == 0 && matrix[2][1] == 0) {
            if (matrix[2][2] == 0) {
                return Result.INTERSECTION;
            } else {
                return Result.SKEW;
            }
        } else {
            return Result.INTERSECTION;
        }
    }

    public enum Result {

        INTERSECTION,
        PARALLEL,
        IDENTICAL,
        SKEW;
    }

    public static void main(String[] args) {
        Line line = new Line(new Vector(1, 0, 1), new Vector(2, 2, 1));
        Line line2 = new Line(new Vector(4, 3, 1), new Vector(1, -2, 2));

        LinePosition position = new LinePosition(line, line2);
        System.out.println(position.resolve());
    }
}
