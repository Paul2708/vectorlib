package de.paul2708.lib.position;

import de.paul2708.lib.line.Line;
import de.paul2708.lib.plane.Plane;
import de.paul2708.lib.point.PlanePointProbe;

/**
 * Created by Paul on 09.07.2016.
 */
public class LinePlanePosition {

    private Plane plane;
    private Line line;

    public LinePlanePosition(Plane plane, Line line) {
        this.plane = plane;
        this.line = line;
    }

    public PlanePointProbe.Result resolve() {
        // Koordinatenform
        /*if (plane.getFormat() == PlaneFormat.COORDINATION_FORMAT) {
            double a = plane.getCoefficients()[0];
            double b = plane.getCoefficients()[1];
            double c = plane.getCoefficients()[2];
            double d = plane.getCoefficients()[3];

            Vector support = line.getSupportVector();
            Vector direction = line.getDirectionVector();

            // double leftSide =
            double t = (d - a*support.getX1() - b*support.getX2() - c*support.getX3()) /
                    (a*direction.getX1() + b*direction.getX2() + c*direction.getX3());

            System.out.println(t);

            if (Double.isInfinite(Math.abs(t))) {
                return Result.PARALLEL;
            } else if (Double.isNaN(Math.abs(t))) {
                return Result.ON_PLAIN;
            } else {
                return Result.INTERSECTION;
            }
        }*/

        return null;
    }

    public enum Result {

        INTERSECTION,
        PARALLEL,
        ON_PLAIN;
    }

    /*public static void main(String[] args) {
        Plane plane = new Plane(4, -5, 0, 11);
        Line line = new Line(Vector.get(2, 4, 1), Vector.get(-2, 2, 1));

        LinePlanePosition position = new LinePlanePosition(plane, line);
        System.out.println(position.resolve());
    }*/
}
