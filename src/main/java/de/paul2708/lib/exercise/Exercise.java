package de.paul2708.lib.exercise;

import de.paul2708.lib.line.Line;
import de.paul2708.lib.plane.Plane;
import de.paul2708.lib.point.Point;

import java.util.Random;

/**
 * Created by Paul on 29.06.2016.
 */
public class Exercise implements Calculatable {

    private String question;
    private Object[] information;

    private static Random random = new Random();

    public Exercise() {}

    public Exercise(String question, Object[] information) {
        this.question = question;
        this.information = information;
    }

    public void print() {
        System.out.println(question);
        String info = "";
        for (int i = 0; i < information.length; i++) {
            info += information[i].toString() + (i+1 == information.length ? "" : "; ");
        }
        System.out.println("Gegeben: " + info);

    }

    @Override
    public Object onCalculate() {
        return null;
    }

    public static class Number {

        private char variable;
        private double value;

        public Number(char variable, double value) {
            this.variable = variable;
            this.value = value;
        }

        @Override
        public String toString() {
            return String.valueOf(variable) + " = " + value;
        }

        public static Number of(char variable, double value) {
            return new Number(variable, value);
        }
    }

    // Random methods
    public static Plane getRandomPlane() {
        try {
            return null;
            // return new Plane(getRandomNumber(-10, 10), getRandomNumber(-10, 10), getRandomNumber(-10, 10), getRandomNumber(-30, 30));
        } catch (IllegalArgumentException e) {
            return getRandomPlane();
        }
    }

    public static Line getRandomLine() {
        return new Line(getRandomPoint(-10, 10), getRandomPoint(-10, 10));
    }

    public static Point getRandomPoint(int min, int max) {
        return Point.get(getRandomNumber(min, max), getRandomNumber(min, max), getRandomNumber(min, max));
    }

    public static double getRandomNumber(int min, int max) {
        return random.nextInt((max - min) + 1) + min;
    }

}
