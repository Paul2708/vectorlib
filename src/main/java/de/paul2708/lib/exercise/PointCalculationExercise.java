package de.paul2708.lib.exercise;

import de.paul2708.lib.line.Line;
import de.paul2708.lib.plane.Plane;
import de.paul2708.lib.point.Point;

/**
 * Created by Paul on 29.06.2016.
 */
public class PointCalculationExercise extends Exercise {

    private boolean isLine;

    private Line line;
    private double t;

    public PointCalculationExercise(Line line, double t) {
        super("Berechne den Punkt auf der Gerade für t = " + t, new Object[] { line } );

        this.line = line;
        this.t = t;

        this.isLine = true;
    }

    private Plane plane;
    private double r;
    private double s;

    public PointCalculationExercise(Plane plane, double r, double s) {
        super("Berechne den Punkt auf der Ebene für r = " + r + " und s = " + s,
                new Object[] { plane, Number.of('r', r), Number.of('s', s) } );

        this.plane = plane;
        this.r = r;
        this.s = s;
    }

    public Point calculate() {
        if (isLine) {
            return line.getPoint(t);
        } else {
            // return plane.getPoint(r, s);
            return null;
        }
    }

}
