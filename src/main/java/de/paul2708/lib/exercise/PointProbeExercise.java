package de.paul2708.lib.exercise;

import de.paul2708.lib.line.Line;
import de.paul2708.lib.point.Point;
import de.paul2708.lib.point.PointProbe;
import de.paul2708.lib.vector.Vector;

/**
 * Created by Paul on 29.06.2016.
 */
public class PointProbeExercise extends Exercise {

    private Line line;
    private Point p;

    public PointProbeExercise(Line line, Point p) {
        super("Liegt der Punkt auf der Geraden?", new Object[] { line, p } );

        this.line = line;
        this.p = p;
    }

    public Boolean calculate() {
        PointProbe probe = new PointProbe(line, p);
        return probe.calculate() == PointProbe.Result.ON_LINE;
    }

    public static void main(String[] args) {
        PointProbeExercise exercise = new PointProbeExercise(new Line(Vector.get(0, 0, 0), Vector.get(0, 1, 0)),
                Point.get(0, 1, 0));
        exercise.print();
        System.out.println(exercise.calculate());
    }
}
