package de.paul2708.lib.exercise;

import de.paul2708.lib.line.Line;
import de.paul2708.lib.position.LinePosition;

/**
 * Created by Paul on 29.06.2016.
 */
public class LinePositionExercise extends Exercise {

    private Line g1;
    private Line g2;

    public LinePositionExercise(Line g1, Line g2) {
        super("Bestimme die Lage beider Geraden zueinander", new Object[] { g1, g2 } );

        this.g1 = g1;
        this.g2 = g2;
    }

    public LinePosition.Result calculate() {
        return new LinePosition(g1, g2).resolve();
    }
}
