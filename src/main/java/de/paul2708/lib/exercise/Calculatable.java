package de.paul2708.lib.exercise;

/**
 * Created by Paul on 25.07.2016.
 */
public interface Calculatable {

    Object onCalculate();
}
