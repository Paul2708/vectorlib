package de.paul2708.lib.exercise;

import de.paul2708.lib.line.Line;
import de.paul2708.lib.position.LinePosition;
import de.paul2708.lib.point.Point;

/**
 * Created by Paul on 29.06.2016.
 */
public class IntersectionExercise extends Exercise {

    private Line g1;
    private Line g2;

    public IntersectionExercise(Line g1, Line g2) {
        super("Berechne den Schnittpunkt der Geraden falls möglich", new Object[] { g1, g2 });

        this.g1 = g1;
        this.g2 = g2;
    }

    public Point calculate() {
        if (g1.getPosition(g2) != LinePosition.Result.INTERSECTION) {
            return Point.get(Double.NaN, Double.NaN, Double.NaN);
        } else {
            return g1.intersect(g2);
        }
    }
}
