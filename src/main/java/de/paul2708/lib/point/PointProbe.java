package de.paul2708.lib.point;

import de.paul2708.lib.line.Line;
import de.paul2708.lib.vector.Vector;

/**
 * Created by Paul on 23.06.2016.
 */
public class PointProbe {

    private Line line;
    private Point point;

    public PointProbe(Line line, Point point) {
        this.line = line;
        this.point = point;
    }

    public Result calculate() {
        double t = 1.0/line.getDirectionVector().getX1()*(point.getX1() - line.getSupportVector().getX1());

        if (Double.isNaN(t)) {
            t = 1.0/line.getDirectionVector().getX2()*(point.getX2() - line.getSupportVector().getX2());
            if (Double.isNaN(t)) {
                t = 1.0/line.getDirectionVector().getX3()*(point.getX3() - line.getSupportVector().getX3());
            }
        }

        if (line.getSupportVector().getX1() + t * line.getDirectionVector().getX1() == point.getX1()) {
            if (line.getSupportVector().getX2() + t * line.getDirectionVector().getX2() == point.getX2()) {
                if (line.getSupportVector().getX3() + t * line.getDirectionVector().getX3() == point.getX3()) {
                    return Result.ON_LINE;
                }
            }
        }

        return Result.OFF_LINE;
    }

    public enum Result {

        ON_LINE,
        OFF_LINE;
    }

    public static void main(String[] args) {
        PointProbe probe = new PointProbe(new Line(new Vector(0, 0, 0), new Vector(0, 1, 0)), new Point(0, 1, 0));
        System.out.println(probe.calculate());
    }
}
