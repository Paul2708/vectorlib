package de.paul2708.lib.point;

import de.paul2708.lib.vector.StationaryVector;

/**
 * Created by Paul on 23.06.2016.
 */
public class Point {

    private double x1;
    private double x2;
    private double x3;

    public Point(double x1, double x2, double x3) {
        this.x1 = x1;
        this.x2 = x2;
        this.x3 = x3;
    }

    public double getX1() {
        return x1;
    }

    public double getX2() {
        return x2;
    }

    public double getX3() {
        return x3;
    }

    public StationaryVector getStationaryVector() {
        return new StationaryVector(this);
    }

    public String getTerm() {
        return "P (" + x1 + "|" + x2 + "|" + x3 + ")";
    }

    @Override
    public String toString() {
        return getTerm();
    }

    public static Point get(double x1, double x2, double x3) {
        return new Point(x1, x2, x3);
    }
}
