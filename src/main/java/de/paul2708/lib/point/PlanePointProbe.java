package de.paul2708.lib.point;

import de.paul2708.lib.plane.Plane;

/**
 * Created by Paul on 28.06.2016.
 */
public class PlanePointProbe {

    private Plane plane;
    private Point point;

    public PlanePointProbe(Plane plane, Point point) {
        this.plane = plane;
        this.point = point;
    }

    public Result calculate() {
        /*if (plane.getFormat() == PlaneFormat.PARAMETER_FORMAT) {
            Vector rSpan = plane.getrSpanVector();
            Vector sSpan = plane.getsSpanVector();

            double[][] matrix = new double[][] { { rSpan.getX1(), sSpan.getX1(), point.getX1() - plane.getSupportVector().getX1() },
                    { rSpan.getX2(), sSpan.getX2(), point.getX2() - plane.getSupportVector().getX2() },
                    { rSpan.getX3(), sSpan.getX3(), point.getX3() - plane.getSupportVector().getX3() }
            };
            LinearSystem.toRREF(matrix);

            if (LinearSystem.resolve(matrix) == LinearSystem.Result.NO_SOLUTION) {
                return Result.OFF_PLAIN;
            } else {
                return Result.ON_PLAIN;
            }
        }'/

        /*if (plane.getFormat() == PlaneFormat.COORDINATION_FORMAT) {
            if (plane.getCoefficients()[0] * point.getX1() + plane.getCoefficients()[1] * point.getX2() +
                    plane.getCoefficients()[2] * point.getX3() == plane.getCoefficients()[3]) {
                return PlanePointProbe.Result.ON_PLAIN;
            } else {
                return PlanePointProbe.Result.OFF_PLAIN;
            }
        }*/

        return null;
    }

    public enum Result {

        ON_PLAIN,
        OFF_PLAIN;
    }

    /*public static void main(String[] args) {
        System.out.println(new PlanePointProbe(new Plane(Vector.get(3, 0, 2), Vector.get(2, 1, 7), Vector.get(3, 2, 5)),
                Point.get(8, 3, 14)).calculate());
    }*/
}
